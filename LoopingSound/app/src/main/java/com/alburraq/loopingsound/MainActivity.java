package com.alburraq.loopingsound;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class MainActivity extends AppCompatActivity {



    private MediaPlayer mediaPlayer;
    private MediaPlayer mediaPlayer2;
    SeekBar seekbar;
    SeekBar seekbar2;
    TextView textview;
    TextView textview2;


    public enum MP_COMMAND {
        START,
        STOP,
        PAUSE
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seekbar = (SeekBar) findViewById(R.id.seekBar1);
        textview = (TextView) findViewById(R.id.text1);


        seekbar2 = (SeekBar) findViewById(R.id.seekBar2);
        textview2 = (TextView) findViewById(R.id.text2);

        seekbar.setMax(10);
        seekbar2.setMax(10);


        mediaPlayer = MediaPlayer.create(this, R.raw.naat);

        mediaPlayer2 = MediaPlayer.create(this, R.raw.noor_wala);

        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                textview.setText("Media: " + i);

//                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, i, 0);
                float v = (float) i / 10.0f;
                Log.e("Vol", "" + v);
                mediaPlayer.setVolume(v, v);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        seekbar2.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                textview2.setText("Media2: " + i);

//                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, i, 0);
                float v = (float) i / 10.0f;
                Log.e("Vol", "" + v);
                mediaPlayer2.setVolume(v, v);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        syncedCommand(mediaPlayer,mediaPlayer2,MP_COMMAND.START);
    }

    /**
     * Uses threads to execute synced commands for the current video media player and
     * background music player in tandem.
     */
    public void syncedCommand(MediaPlayer player1, MediaPlayer player2, MP_COMMAND command) {
        final CyclicBarrier commandBarrier = new CyclicBarrier(2);
        new Thread(new SyncedCommandService(commandBarrier, player1, command)).start();
        new Thread(new SyncedCommandService(commandBarrier, player2, command)).start();
    }


    /**
     * Inner class that starts a given media player synchronously
     * with other threads utilizing SyncedStartService
     */
    private class SyncedCommandService implements Runnable {
        private final CyclicBarrier              mCommandBarrier;
        private       MainActivity.MP_COMMAND mCommand;
        private       MediaPlayer                mMediaPlayer;

        public SyncedCommandService(CyclicBarrier barrier, MediaPlayer player, MainActivity.MP_COMMAND command) {
            mCommandBarrier = barrier;
            mMediaPlayer = player;
            mCommand = command;
        }

        @Override public void run() {
            try {
                mCommandBarrier.await();
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }

            switch (mCommand) {
                case START:
                    mMediaPlayer.start();
                    break;

                case STOP:
                    mMediaPlayer.stop();
                    break;

                case PAUSE:
                    mMediaPlayer.pause();
                    break;

                default:
                    break;
            }
        }
    }
}
