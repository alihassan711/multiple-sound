package com.alburraq.loopingsound;


import android.app.Activity;
import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
/**
 * Created by Al-Burraq-Dev on 6/5/2017.
 */

public class SoundPoolTest extends Activity {

    SoundPool soundPool;
    AudioManager audioManager;
    int soundId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        //the maximum number of simultaneous streams for this SoundPool object
        int maxStreams = 4;
        //the audio stream type as described in AudioManager
        int streamType = AudioManager.STREAM_MUSIC;
        //the sample-rate converter quality. Currently has no effect. Use 0 for the default.
        int srcQuality = 0;

        soundPool = new SoundPool(maxStreams, streamType, srcQuality);
        soundPool.setOnLoadCompleteListener(soundPoolOnLoadCompleteListener);
        soundId = soundPool.load(this, R.raw.noor_wala, 1);

    }

    OnLoadCompleteListener soundPoolOnLoadCompleteListener =
            new OnLoadCompleteListener() {

                @Override
                public void onLoadComplete(SoundPool soundPool,
                                           int sampleId, int status) {
                    if (status == 0) {
                        float vol = audioManager.getStreamVolume(
                                AudioManager.STREAM_MUSIC);
                        float maxVol = audioManager.getStreamMaxVolume(
                                AudioManager.STREAM_MUSIC);
                        float leftVolume = vol / maxVol;
                        float rightVolume = vol / maxVol;
                        int priority = 1;
                        int no_loop = 0;
                        float normal_playback_rate = 1f;
                        soundPool.play(soundId,
                                leftVolume,
                                rightVolume,
                                priority,
                                no_loop,
                                normal_playback_rate);
                    } else {
                        Toast.makeText(SoundPoolTest.this,
                                "SoundPool.load() fail",
                                Toast.LENGTH_LONG).show();
                    }

                }
            };
}